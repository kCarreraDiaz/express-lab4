var express = require('express');
var app = express();

// Rutas
app.get('/', c_inicio);
app.get('/clientes', c_cliente);
app.get('/productos', c_productos);


// Funciones que me rendirizan la vista
function c_inicio(req, res) {
    res.send('<h1>Express INICIO</h1>')
}

function c_cliente(req, res) {
    res.send('<li>Carrera Diaz Katherine</li><li>Juan perez</li><li>Jose Rodriguez</li>')
}
function c_productos(req, res) {
    res.send('<li>TV</li><li>Celular</li><li>Tablet</li>')
}

function c_server(req, res) {
    console.log('port:5000');
}

var server = app.listen(5000, c_server);